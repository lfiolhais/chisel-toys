package example.fsm

import chisel3._
import chisel3.util._

class VendingMachine extends Module {
  val io = IO(new Bundle {
    val nickel = Input(Bool())
    val dime   = Input(Bool())
    val valid  = Output(Bool())
  })
  // Implement HERE
}

object ALU extends App {
  chisel3.Driver.execute(args, () => new VendingMachine())
}
