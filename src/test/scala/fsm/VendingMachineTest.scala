package example.fsm

import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

class VendingMachineUnitTester(c: VendingMachine) extends PeekPokeTester(c) {
  var money = 0
  var isValid = false
  for (t <- 0 until 20) {
    val coin     = rnd.nextInt(3)*5
    val isNickel = coin == 5
    val isDime   = coin == 10

    // Advance circuit
    poke(c.io.nickel, if (isNickel) 1 else 0)
    poke(c.io.dime,   if (isDime) 1 else 0)
    step(1)

    // Advance model
    money = if (isValid) 0 else money + coin
    isValid = money >= 20

    // Compare
    expect(c.io.valid, if (isValid) 1 else 0)
  }
}

class VendingMachineTester extends ChiselFlatSpec {
  private val backendNames = if(firrtl.FileUtils.isCommandAvailable("verilator")) {
    Array("firrtl", "verilator", "treadle")
  }
  else {
    Array("firrtl")
  }
  for ( backendName <- backendNames ) {
    "VendingMachine" should s"store random data (with $backendName)" in {
      Driver(() => new VendingMachine(), backendName) {
        c => new VendingMachineUnitTester(c)
      } should be (true)
    }
  }
}
