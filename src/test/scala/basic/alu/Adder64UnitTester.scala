package example.basic.alu

import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

import math.BigInt

class Adder64UnitTester(c: Adder64) extends PeekPokeTester(c) {
  val n_tests = rnd.nextInt(1000) * 10

  for (i <- 0 until n_tests) {
    // Poke inputs
    val operand_A = BigInt(64, rnd)
    val operand_B = BigInt(64, rnd)
    val c_in      = BigInt(1, rnd)

    poke(c.io.operands(0), operand_A)
    poke(c.io.operands(1), operand_B)
    poke(c.io.cin, c_in)

    // Advance simulation two cycles
    step(1)

    // Perform the same computation in scala so that we
    // can compare results with the simulation
    val result = operand_A + operand_B + c_in

    println("Added: " + operand_A + " + " + operand_B + " + " + c_in)
    expect(c.io.result, result & BigInt("0ffffffffffffffff", 16))
    expect(c.io.cout  , (result & BigInt("010000000000000000", 16)) >> 64)
  }
}

class Adder64Tester extends ChiselFlatSpec {
  private val backendNames = if(firrtl.FileUtils.isCommandAvailable("verilator")) {
    Array("firrtl", "verilator")
  }
  else {
    Array("firrtl")
  }
  for ( backendName <- backendNames ) {
    "Adder64" should s"store random data (with $backendName)" in {
      Driver(() => new Adder64(), backendName) {
        c => new Adder64UnitTester(c)
      } should be (true)
    }
  }
}
