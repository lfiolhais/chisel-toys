package example.sm

import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

class SignMagnitudeUnitTester(c: Mac[SignMagnitude]) extends PeekPokeTester(c) {
  // Implement HERE
}

class SignMagnitudeTester extends ChiselFlatSpec {
  implicit object SignMagnitudeRingImpl extends SignMagnitudeRing
  private val backendNames = if(firrtl.FileUtils.isCommandAvailable("verilator")) {
    Array("firrtl", "verilator", "treadle")
  }
  else {
    Array("firrtl")
  }
  for ( backendName <- backendNames ) {
    "SignMagnitude" should s"store random data (with $backendName)" in {
      Driver(() => new Mac(new SignMagnitude(Some(4)), new SignMagnitude(Some(5))), backendName) {
        c => new SignMagnitudeUnitTester(c)
      } should be (true)
    }
  }
}
