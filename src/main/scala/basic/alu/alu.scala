package example.basic.alu

import chisel3._

class ALU() extends Module {
  val io = IO(new Bundle {})
  // Implement HERE
}

// This is needed to generate the verilog just for this module. When generating
// the verilog this object will only be needed in the top module.
object ALU extends App {
  chisel3.Driver.execute(args, () => new ALU())
}
