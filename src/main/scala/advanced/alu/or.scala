package example.advanced.alu

import chisel3._

class Or(width: Int) extends AluOp(width) {
  io.result := io.operands(0).asUInt | io.operands(1).asUInt
}
