package example.basic.alu

import chisel3._

class Adder() extends Module {
  val io = IO(new Bundle {
    // Inputs
    val operand_a = Input(UInt(32.W))
    val operand_b = Input(UInt(32.W))
    val cin = Input(UInt(1.W))

    // Outputs
    val result = Output(UInt(32.W))
    val cout = Output(Bool())
  })
  // Implement HERE
}

object Adder extends App {
  chisel3.Driver.execute(args, () => new Adder())
}
