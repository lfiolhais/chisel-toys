package example.basic.alu

import chisel3._

class Adder64() extends Module {
  val io = IO(new Bundle {
    // Inputs
    val operands = Input(Vec(2, UInt(64.W)))
    val cin = Input(UInt(1.W))

    // Outputs
    val result = Output(UInt(64.W))
    val cout = Output(Bool())
  })
  // Implement HERE
}

object Adder64 extends App {
  chisel3.Driver.execute(args, () => new Adder64())
}
