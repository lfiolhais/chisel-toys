package example.basic.alu

import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

import math.BigInt

class AdderUnitTester(c: Adder) extends PeekPokeTester(c) {
  val n_tests = rnd.nextInt(1000) * 10

  for (i <- 0 until n_tests) {
    // Poke inputs
    val operand_A = rnd.nextInt & 0x7fffffff
    val operand_B = rnd.nextInt & 0x7fffffff
    val c_in      = rnd.nextInt(2) == 0

    poke(c.io.operand_a, operand_A)
    poke(c.io.operand_b, operand_B)
    poke(c.io.cin, c_in)

    // Advance simulation two cycles
    step(1)

    // Perform the same computation in scala so that we
    // can compare results with the simulation
    val result = operand_A.toLong + operand_B.toLong + c_in.toLong

    expect(c.io.result, result & 0xffffffff)
    expect(c.io.cout  , (result & 0x10000000) >> 32)
  }
}

class AdderTester extends ChiselFlatSpec {
  private val backendNames = if(firrtl.FileUtils.isCommandAvailable("verilator")) {
    Array("firrtl", "verilator")
  }
  else {
    Array("firrtl")
  }
  for ( backendName <- backendNames ) {
    "Adder" should s"store random data (with $backendName)" in {
      Driver(() => new Adder(), backendName) {
        c => new AdderUnitTester(c)
      } should be (true)
    }
  }
}
