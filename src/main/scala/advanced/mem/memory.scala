package example.advanced.mem

import chisel3._

import scala.math.pow

class MemIO[+T <: Data](gen: T) extends Bundle {
  // Inputs
  val data_in       = Input(gen)
  val addr_w        = Input(gen)
  val addr_r        = Input(gen)
  val we            = Input(Bool())

  override def cloneType: this.type = {
    new MemIO(gen).asInstanceOf[this.type];
  }
}

class Memory[T <: Data](gen: T) extends Module {
  val io = IO(new Bundle {
                val in = new MemIO(gen)
                val data_out = Output(gen)
              })

  val my_mem = SyncReadMem(1024, gen)
  val addr_w = io.in.addr_w.asUInt
  val addr_r = io.in.addr_r.asUInt

  when (io.in.we) {
    my_mem.write(addr_w(9, 0), io.in.data_in)
    io.data_out := 0.U
  } .otherwise {
    io.data_out := my_mem.read(addr_r(9, 0))
  }
}
