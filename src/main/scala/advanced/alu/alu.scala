package example.advanced.alu

import chisel3._
import chisel3.util._

import scala.collection.mutable.ArrayBuffer

import example.advanced.mem.MemIO

class AluIO(val my_width: Int) extends Bundle {
  // Inputs
  val sel_A   = Input(Bool())
  val input_A = Input(UInt(my_width.W))
  val input_B = Input(UInt(my_width.W))
  val sel_op  = Input(UInt(3.W))
}

class ALU(val input_w: Int) extends Module {
  val io = IO(new Bundle {
                // Inputs
                val in  = new AluIO(input_w)

                // Outputs
                val mem = Flipped(new MemIO(UInt(input_w.W)))
              })

  require((input_w & (input_w - 1)) == 0 && input_w >= 32,
          "Input width must be power of two and greater than 32")

  // MUXs
  val operand_A = Mux(io.in.sel_A, io.in.input_A, io.in.input_B)
  val operand_B = Mux(io.in.sel_A, io.in.input_B, io.in.input_A)

  // Registers
  val r0 = RegNext(operand_A)
  val r1 = RegNext(operand_B)
  val r2 = RegNext(io.in.sel_op)

  private def connectALU(comp: AluOp, opA: UInt, opB: UInt) {
    comp.io.operands(0) := opA
    comp.io.operands(1) := opB
  }

  // Create Functional Units
  val units = new ArrayBuffer[AluOp]
  val adder = Module(new AnyAdder(input_w))
  // Connect carry in manually
  adder.io.cin := r2(0)
  units += adder
  units += Module(new And(input_w))
  units += Module(new Or(input_w))
  units += Module(new Xor(input_w))

  // Connect all FUs to their inputs
  units.foreach(unit => connectALU(unit, r0, r1))

  // Get list of all io.results
  val results = units.zipWithIndex.map {
    case(unit, x) => (x.U -> unit.io.result)
  }

  // Output
  val out_mux = MuxLookup(r2, 0.U, results)

  val r3 = RegInit(0.U(input_w.W))

  when (!out_mux(0)) {
    r3 := out_mux
  }

  io.mem.data_in := r3
  io.mem.we      := !out_mux(0)
  io.mem.addr_r  := out_mux
  io.mem.addr_w  := (r3 ^ "h_ffff".U) >> 14
}

// This is needed to generate the verilog just for this module. When generating
// the verilog this object will only be needed in the top module.
object ALU extends App {
  // Change width here when generating verilog
  val width = 32

  chisel3.Driver.execute(args, () => new ALU(width))
}
