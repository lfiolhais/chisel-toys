package example.advanced.alu

import chisel3._

class AluOpIO(val my_width: Int) extends Bundle {
  // Inputs
  val operands = Input(Vec(2, UInt(my_width.W)))

  // Outputs
  val result = Output(UInt(my_width.W))
}

class AddOpIO(override val my_width: Int) extends AluOpIO(my_width) {
  // Inputs
  val cin = Input(UInt(1.W))

  // Outputs
  val cout = Output(Bool())
}

abstract class AluOp(val my_width: Int) extends Module {
  lazy val io =  IO(new AluOpIO(my_width))
}
