package example.fsm

import chisel3._

object VendingMachineMain extends App {
  iotesters.Driver.execute(args, () => new VendingMachine()) {
    c => new VendingMachineUnitTester(c)
  }
}

object VendingMachineRepl extends App {
  iotesters.Driver.executeFirrtlRepl(args, () => new VendingMachine())
}
