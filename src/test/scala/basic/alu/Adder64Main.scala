package example.basic.alu

import chisel3._

object Adder64Main extends App {
  iotesters.Driver.execute(args, () => new Adder64) {
    c => new Adder64UnitTester(c)
  }
}

object Adder64Repl extends App {
  iotesters.Driver.executeFirrtlRepl(args, () => new Adder64)
}
