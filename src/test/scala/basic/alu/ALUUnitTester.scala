package example.basic.alu

import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

import math.BigInt

class ALUUnitTester(c: ALU) extends PeekPokeTester(c) {
  // Implement this
}

class ALUTester extends ChiselFlatSpec {
  private val backendNames = if(firrtl.FileUtils.isCommandAvailable("verilator")) {
    Array("firrtl", "verilator")
  }
  else {
    Array("firrtl")
  }
  for ( backendName <- backendNames ) {
    "ALU" should s"store random data (with $backendName)" in {
      Driver(() => new ALU(), backendName) {
        c => new ALUUnitTester(c)
      } should be (true)
    }
  }
}
