package example.advanced.alu

import chisel3._
import chisel3.util.Cat

class AnyAdder(width: Int) extends AluOp(width) {
  override lazy val io =  IO(new AddOpIO(width))

  // Invert operand_b when subtracting
  val operand_b = Wire(UInt(width.W))
  when (io.cin === 1.U) {
    operand_b := ~io.operands(1)
  } .otherwise {
    operand_b := io.operands(1)
  }

  // Generate Adders
  private val adders = Seq.fill(width/32)(Module(new Adder))

  // Connect Adders
  for (i <- 0 until (width/32)) {
    val cin = if (i == 0) {
      io.cin
    } else {
      adders(i - 1).io.cout.asUInt
    }

    adders(i).io.cin       := cin
    adders(i).io.operand_a := io.operands(0)(31 + i * 32, i * 32)
    adders(i).io.operand_b := operand_b(31 + i * 32, i * 32)
  }

  // Final result
  val results = VecInit(adders.map(adder => adder.io.result).reverse)
  io.result := Cat(results)
  io.cout   := adders((width/32) - 1).io.cout
}
