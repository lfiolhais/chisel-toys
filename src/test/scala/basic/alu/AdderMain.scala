package example.basic.alu

import chisel3._

object AdderMain extends App {
  iotesters.Driver.execute(args, () => new Adder) {
    c => new AdderUnitTester(c)
  }
}

object AdderRepl extends App {
  iotesters.Driver.executeFirrtlRepl(args, () => new Adder)
}
